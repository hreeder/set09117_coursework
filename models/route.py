from collections import deque


class Route(object):
    def __init__(self, capacity=50):
        """
        Constructor for Route
        :param capacity: (Optional) max capacity for the route, default to 50
        :return: new Route
        """
        # We're gonna keep a deque for order, and a set for membership
        self.route = deque()
        self.customers = set()

        # Also, keep a track of the current load. We start with nothing on the route, so load is 0
        self.load = 0
        self.capacity = capacity

    def add_at_beginning(self, node):
        self.route.appendleft(node)
        self.load += node.attributes['requirement']
        self.customers.add(node)

    def add(self, node):
        if self.has_capacity(node['requirement']):
            self.route.append(node)
            self.load += node.attributes['requirement']
            self.customers.add(node)
        else:
            raise Exception

    def head(self):
        return self.route[0]

    def tail(self):
        return self.route[-1]

    def has_capacity(self, requested):
        if (self.load + requested) <= self.capacity:
            return True
        else:
            return False

    def merge(self, other_route):
        if self.has_capacity(other_route.load):
            for node in other_route.route:
                self.add(node)
        else:
            raise Exception("Butts no")

    def __contains__(self, item):
        return item in self.customers

    def __repr__(self):
        output = "<Route - "
        nodes = [node.__repr__() for node in self.route]
        output += ", ".join(nodes)
        output += " - Load: %d/%d" % (self.load,self.capacity)
        return output
