class Node(object):
    def __init__(self):
        self.attributes = {}
        self.connections = []

    def connect(self, edge):
        self.connections.append(edge)

    def __str__(self):
        lines = []

        for key in self.attributes:
            lines.append(key + " = " + str(self.attributes[key]))

        return "\n".join(lines)

    def __repr__(self):
        return "<Node at %d %d>" % (self.attributes['x'], self.attributes['y'])

    def __getitem__(self, item):
        return self.attributes.__getitem__(item)

    def __setitem__(self, key, value):
        return self.attributes.__setitem__(key, value)