class Edge(object):
    def __init__(self, node1, node2, weight):
        self.node1 = node1
        self.node2 = node2
        self.weight = weight

    def navigate(self, this_end):
        if this_end == self.node1:
            return self.node2
        else:
            return self.node1
