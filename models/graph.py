from models import Edge


class Graph(object):
    def __init__(self):
        self.nodes = []

    def add_node(self, node):
        self.nodes.append(node)

    def connect(self, node1, node2, weight):
        edge = Edge(node1, node2, weight)
        node1.connect(edge)
        node2.connect(edge)

    def __str__(self):
        lines = []
        for node in self.nodes:
            lines.append(str(node))

        return "\n".join(lines)
