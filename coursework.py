from models import Graph, Node, Route

import csv
import timeit
import math
import os

CSI = "\033["
GREEN = CSI + "92m"
BLUE = CSI + "94m"
RED = CSI + "91m"
RESET = CSI + "0m"

def printline(tag, line, color=RESET):
    tag = BLUE + "[" + tag + "]" + RESET
    line = color + line + RESET
    print(" ".join([tag, line]))

def read_data(filename):
    with open(os.path.join('testdata', filename), 'rb') as csvfile:
        datareader = csv.reader(csvfile)
        depot = datareader.next()
        customers = []
        for row in datareader:
            customers.append(row)

        return depot, customers


def write_data(filename, routes):
    with open(problem + "answer.csv", 'wb') as outputfile:
        writer = csv.writer(outputfile, quoting=csv.QUOTE_MINIMAL)
        for route in routes:
            line = []
            for node in route.route:
                line.append(node['x'])
                line.append(node['y'])
                line.append(node['requirement'])

            writer.writerow(line)


def get_distance(node1, node2):
    """
    get_distance returns the pythagorean distance between two nodes
    :param node1: The source node
    :param node2: The destination node
    :return: The distance between node1 and node2
    """
    x1 = node1.attributes['x']
    x2 = node2.attributes['x']
    y1 = node1.attributes['y']
    y2 = node2.attributes['y']

    xd = x2-x1
    yd = y2-y1

    return math.sqrt(xd*xd + yd*yd)


def clarke_wright(graph):
    customers = filter(lambda n: not n.attributes['depot'], graph.nodes)        # Filter out any non-depot nodes
    depot = filter(lambda n: n.attributes['depot'], graph.nodes)[0]             # We can assume there is only one depot
                                                                                # so access element 0

    savings = []

    # Calculate the savings between a pair of nodes for all nodes in the graph
    for node in customers:
        for other_node in customers:
            if other_node != node:
                saving = (get_distance(depot, node) + get_distance(depot, other_node)) - get_distance(node, other_node)
                savings.append({
                    'to': other_node,
                    'from': node,
                    'saving': saving
                })

    # Now sort the list of savings. We need reverse=True to go from largest to smallest
    savings = sorted(savings, key=lambda saving: saving['saving'], reverse=True)

    routes = []

    # Loop through all the savings we've calculated
    for saving in savings:
        c_from = saving['from']
        c_to = saving['to']

        from_in_route = False
        to_in_route = False
        # I'm going to need to know if either the from or to nodes appear in a route already
        for route in routes:
            if not from_in_route and c_from in route:
                from_in_route = True
            if not to_in_route and c_to in route:
                to_in_route = True

            # Stop this inner loop if we've found both of them in a route already
            if from_in_route and to_in_route:
                break

        # If neither customer is in a route already
        if not from_in_route and not to_in_route:
            if c_from['requirement'] + c_to['requirement'] <= depot['capacity']:
                route = Route(depot['capacity'])
                route.add(c_from)
                route.add(c_to)
                routes.append(route)
        else:
            # Find a route that ends at from
            if not to_in_route:
                for route in routes:
                    if c_from == route.tail() and route.has_capacity(c_to['requirement']):
                        route.add(c_to)
                        break
            elif not from_in_route:
                for route in routes:
                    if c_to == route.tail() and route.has_capacity(c_from['requirement']):
                        route.add_at_beginning(c_from)
                        break

        # Route Merging Time
        merged = None
        for route_x in routes:
            if merged:
                break

            # If the end of our route is our from node
            if route_x.tail() == c_from:
                for route_y in routes:
                    # and the start of another route is our to node
                    if route_y.head() == c_to:
                        # Merge them, if there is capacity and we can
                        if not route_x == route_y and route_x.has_capacity(route_y.load):
                            route_x.merge(route_y)
                            merged = route_y
                            break

        if merged:
            # Remove the route we merged too
            routes.remove(merged)

    # Now go through all customers, if they're not in a route, add them to a route solely for them
    for cust in customers:
        in_route = False
        for route in routes:
            if cust in route:
                in_route = True

        if not in_route:
            r = Route(depot['capacity'])
            r.add(cust)
            routes.append(r)

    return routes


def coursework(problem):
    # Read in our problem
    raw_depot, raw_customers = read_data(problem + "prob.csv")
    printline("Coursework", "Problem Loaded")

    # Create our graph and populate our depot
    g = Graph()
    depot = Node()
    depot['x'] = int(raw_depot[0])
    depot['y'] = int(raw_depot[1])
    depot['capacity'] = int(raw_depot[2])
    depot['depot'] = True

    g.add_node(depot)

    # Loop through all customers, create nodes for them and place them into our graph
    for raw_customer in raw_customers:
        customer = Node()
        customer['x'] = int(raw_customer[0])
        customer['y'] = int(raw_customer[1])
        customer['requirement'] = int(raw_customer[2])
        customer['depot'] = False

        g.add_node(customer)

    printline("Coursework", "Graph set up")

    # Use Clarke Wright to generate routes
    printline("Coursework", "Generating Routes...", RED)
    routes = clarke_wright(g)
    printline("Coursework", "Routes Generated", GREEN)

    write_data(problem + "answer.csv", routes)
    printline("Coursework", "Answer Written")


def runner(func, *args, **kwargs):
    def wrapped():
        return func(*args, **kwargs)
    return wrapped

if __name__ == "__main__":
    # This section to run all problems with timeit
    timings = []
    timings.append(["# Nodes", "Total", "Average", "# Runs"])

    for (dirpath, dirnames, filenames) in os.walk("testdata"):
        for filename in filenames:
            if filename.endswith("prob.csv"):
                n = 50
                problem = filename[:-8]
                print "Running Problem" + problem
                runthis = runner(coursework, problem)
                time = timeit.timeit(runthis, number=n)
                print "Problem %s took %ss to run %s times" % (problem, time, n)
                timings.append([filename[4:-8], time, time/n, n])

    with open('timings.csv', 'wb') as timingscsv:
        writer = csv.writer(timingscsv, quoting=csv.QUOTE_MINIMAL)
        for line in timings:
            writer.writerow(line)


    # problem = "rand00050"

    # # This section for running a single problem with timeit
    # runthis = runner(coursework, problem)
    # time = timeit.timeit(runthis, number=1)
    # output = "Problem " + GREEN + "%s" + RESET + " took " + GREEN + "%s" + RESET + " to run"
    # print output % (problem, time)

    # coursework(problem)
