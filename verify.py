#!/usr/bin/env python
import os
for (dirpath, dirnames, filenames) in os.walk('.'):
    for filename in filenames:
        if filename.endswith('answer.csv'):
            prob = filename[:-10]
            print '==== Verifying %s ====' % (prob,)
            os.system('java -jar supplied/Verify.jar testdata/%sprob.csv %s' % (prob, filename))
